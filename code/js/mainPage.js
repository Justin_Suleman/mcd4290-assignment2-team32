// Code for the main app page (Past Runs list).

function viewRun(runIndex){ // Function that will be called when the saved run has been selected from the main page
    
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex); // Save the selected run to local storage so it can be accessed from View Run page.
    
    location.href = 'viewRun.html'; // Loads the view run page
    
}

if(localLoad != null){ // Creates an IF statement to check if the array is empty
    
  var mainOutput = JSON.parse(localStorage.getItem('runArray')); // Retrieves the parsed array of runs from local storage into a variable

  document.getElementById('mine').innerHTML = mainOutput[0]._runTitle; // Displays the first run name on the main page
    
 }

var indexOutput = JSON.parse(localStorage.getItem('runArray')); // Parses the array containing saved runs from local storage

let listHTML = ''; // Sets the main page to blank

for(i=0 ; i<indexOutput.length ; i++){ // Creates a FOR loop that iterates over all the runs in the saved array
   
  // Adds the name of the saved run and the save date into the variable as a table cell DIV  
  listHTML += "<tr> <td onmousedown=\"listRowTapped("+i+")\" class=\"full-width mdl-data-table__cell--non-numeric\">" + indexOutput[i]._runTitle + " &rarr; " + indexOutput[i]._day;
    
  // Adds the run duration of the saved run into the variable as a subtitle DIV   
  listHTML += "<div class=\"subtitle\">" + "Run Time: " + indexOutput[i]._time +"</div>";
    
  // Adds the total distance of the saved run into the variable as a subtitle DIV    
  listHTML += "<div class=\"subtitle\">" + "Distance Travelled: " + indexOutput[i]._distanceTravelled + ' m' + "</div></td></tr>";
    
  document.getElementById('mine').innerHTML = listHTML; // Displays the list of runs and additional information on the main page
    
  }

function listRowTapped(i) // Function that will be called when the selected run has been pressed on the main page
{
    let key1 = 'runIndex'; // Sets the key as the index of the run 
    
    localStorage.setItem(key1,i); // Saves the index of the run into local storage
    
    window.location.href = 'viewRun.html'; // Loads the view run page
}