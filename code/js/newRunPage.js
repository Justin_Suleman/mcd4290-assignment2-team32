// Code for the Measure Run page.

// List of global variables that will be used in the code
var valLatitude 
var valLongitude
var currentPosition
var lngLat
var newlngLat
var key
var saveBox
var day
var sec, min, hour
var timer 
var accu
var valAcc
var distanceRemainder
var remainderLatitude
var remainderLongitude
var totalDistance
var newLongitude
var newLatitude
var id
var target
var options
var popup

function success(pos) { // Function that will be called in the new run page to determine the current user location

  valLatitude = Number(pos.coords.latitude) // Updates the latitude value to the user's current location
  
  valLongitude = Number(pos.coords.longitude) // Updates the longitude value to the user's current location
  
  accu = pos.coords.accuracy // Calls built-in function to calculate the location accuracy
    
  valAcc = document.getElementById('acc') // Sets a variable to set the DIV for showing accuracy
    
  valAcc.innerHTML = accu // Displays the accuracy in the NewRun webpage

  // Creates an IF statement to calculate remaining distance only if the destination coordinates are defined.  
  if(newLatitude != undefined && newLongitude != undefined){ 
   
    // Calculates the remaining horizontal and vertical distance by subtracting the coordinates of the destination with the current location  
    remainderLongitude = Math.abs((newLongitude-valLongitude))
    remainderLatitude = Math.abs((newLatitude-valLatitude))
  
    // Calculates the remaining distance by finding the hypotenuse from the vertical and horizontal distance, and converting to meters  
    distanceRemainder = ((Math.sqrt((Math.pow(remainderLongitude,2)) + (Math.pow(remainderLatitude,2))))*150000).toFixed(2)
  
    document.getElementById('remainingDistance').innerHTML = distanceRemainder + " m" // Displays the remaining distance of the run 
  }
    
}

// Sets the built-in accuracy for getting user location
options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};

id = navigator.geolocation.watchPosition(success); // Calls the function sucess everytime user move  

var attemptCheck = localStorage.getItem('attempt') // Retrieves the reattempt check value from local storage into a variable

var initialise = localStorage.getItem('runArray') // Retrieves the array of runs from local storage into a variable

var indexCheck = localStorage.getItem('runIndex') // Retrieves the index of the specific run from local storage into a variable

if(attemptCheck === '1'){ // Creates an IF statement to check if the reattempt signal variable is set
  
   document.getElementById('checkAttempt').disabled = false // Enables the 'Check Attempt' button
   document.getElementById('randomDestination').disabled = true // Disables the 'Create Run' button
   document.getElementById('moveToLocation').disabled = true //  Disables the 'Move To Location' button
   
    }

if(attemptCheck === 'a'){ // Creates an IF statement to check if the reattempt signal variable is not set
    
        document.getElementById('moveToLocation').disabled = false //  Disables the 'Move To Location' button
        document.getElementById('checkAttempt').disabled = true // Enables the 'Check Attempt' button
    
    }

function checkAttempt(){ // Function that will be called when the 'Reattempt Run' button is pressed
    
  attemptCheck = localStorage.getItem('attempt') // Retrieves the value of the reattempt signal from local storage into a variable

  var indexCheck = localStorage.getItem('runIndex') // Retrieves the value of the run index from local storage into a variable

  var arrayCheck = JSON.parse(localStorage.getItem('runArray')) // Retrieves the parsed run array from local storage into a variable
    
  moveToLocation() // Calls the moveToLocation() to zoom to the user location
   
  document.getElementById('randomDestination').disabled = true // Disables the 'Create Run' button
    
  var attemptDistance // Creates a variable that will store the distance of the selected run  from the saved array
  
  // Calculates the longitudinal and latitudinal distance of the user from the start location of the saved run
  var attemptLong = Math.pow((valLatitude - arrayCheck[indexCheck]._startLocation.latitude),2)
  var attemptLat = Math.pow((valLongitude - arrayCheck[indexCheck]._startLocation.longitude),2)
  
  // Calculates the distance of the current user location from the saved current location using Hypotenuse triangle formula in meters
  attemptDistance = Math.sqrt(attemptLat+attemptLong)*150000
 
  if(attemptDistance>20){ // Checks if the distance of the current and save current location is greater than 20m
    
    document.getElementById('startRun').disabled = true; // Disables the 'Start Run' button
    
   }
    
  if(attemptDistance<20){ // Checks if the distance of the current and save current location is less than 20m
        
     moveToLocation() // Calls the moveToLocation() function to pan to the user location
        
     document.getElementById('randomDestination').disabled = true; // Disables the 'Create Run' button
      
     document.getElementById('startRun').disabled = false; // Enables the 'Start Run' button
        
     attemptCheck = 'a' // Sets the check variable into a string to avoid activating the reattempt when starting a new run
        
     localStorage.setItem('attempt',attemptCheck) // Saves the updated value of the attemptCheck variable into local storage
        
     var todayReattempt = new Date(); // Creates a variable to get the current date
    
     day = todayReattempt.getFullYear()+'-'+(todayReattempt.getMonth()+1)+'-'+todayReattempt.getDate() // Simplifies the format of date to YY-MM-DD
        
     totalDistance = arrayCheck[indexCheck]._totalDistance // Retrieves the total distance of the run from the saved array
        
     document.getElementById('distance').innerHTML += totalDistance + " m" // Displays the total distance of the previous destination
        
     // Creates a popup on the map to display the destination location 
     popup = new mapboxgl.Popup({closeOnClick: false})
     .setLngLat([arrayCheck[indexCheck]._destinationLocation.longitude,arrayCheck[indexCheck]._destinationLocation.latitude])
     .setHTML('New Destination')
     .addTo(map);
   
   // Checks if the destination coordinates of the selected saved run exists  
   if(arrayCheck[indexCheck]._destinationLocation.latitude != undefined && arrayCheck[indexCheck]._destinationLocation.longitude != undefined){ 
   
     // Calculates the remaining horizontal and vertical distance by subtracting the coordinates of the destination with the current location  
     remainderLongitude = Math.abs((arrayCheck[indexCheck]._destinationLocation.longitude-valLongitude))
     remainderLatitude = Math.abs((arrayCheck[indexCheck]._destinationLocation.latitude-valLatitude))
  
     // Calculates the remaining distance by finding the hypotenuse from the vertical and horizontal distance, and converting to meters  
     distanceRemainder = ((Math.sqrt((Math.pow(remainderLongitude,2)) + (Math.pow(remainderLatitude,2))))*150000).toFixed(2)
  
     document.getElementById('remainingDistance').innerHTML = distanceRemainder + " m" // Displays the remaining distance in the corresponding DIV
     }
    
    // Sets the newlngLat object to conatin properties based on the longitude and latitude destination of previous run  
    newlngLat = {
            latitude: arrayCheck[indexCheck]._destinationLocation.latitude,
            longitude: arrayCheck[indexCheck]._destinationLocation.longitude
    }
   } 
  }

function moveToLocation(){ // Function that will be called when the 'Move to Location' button is pressed
    
 if(accu > 20){ // Creates an IF statement to check if the accuracy drops below 20m
     
    document.getElementById('accuracyWarning').innerHTML = 'Location accuracy is too low!'; // Displays the message into the corresponding DIV
    document.getElementById('randomDestination').disabled = true; // Disables the 'Create Run' button
    
    } else{
        
    document.getElementById('accuracyWarning').innerHTML = ''; // Do not display anything in the corresponding DIV
    document.getElementById('randomDestination').disabled = false; // Enables the 'Create Run' button
                 
             } 
            
 currentPosition = new mapboxgl.LngLat(valLongitude,valLatitude); // Creates a coordinate variable of the current location to be used in the map
            
 map.panTo(currentPosition) // Pans the screen to move to the user location
            
 lngLat = { // Creates an object containing the coordinates of the current user location as properties
    latitude: valLatitude,
    longitude: valLongitude
 }
  
 // Creates a popup on the map to display the user current location   
 var popupCurrent = new mapboxgl.Popup({offset: 20, className: 'my-class'})
        .setLngLat([valLongitude,valLatitude])
        .setText('Current Location')
        .addTo(map);
             
 document.getElementById('randomDestination').removeAttribute("disabled") // Enables the 'Create Run' button
            
        }

function randomDestination() { // Function that will be called when the 'Create Run' button is pressed
    
 document.getElementById('distance').innerHTML = ''; // Removes any previous text in the 'Total distance' DIV
    
 if (accu  <= 20 ){   // Creates an IF statement to check the accuracy for the start run button to be enabled
     
    document.getElementById('startRun').disabled=false; // Enables the 'Start Run' button
     
 } else{
     
    document.getElementById('startRun').disabled=true; // Disables the 'Start Run' button
 } 
    
 if(popup != undefined){ // Checks for any pre-existing popup of the previous destination location
     
   popup.remove() // Removes the popup
     
   }
    
 var today = new Date(); // Creates a variable to get the current date
    
 day = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate() // Simplifies the format of date to YY-MM-DD
    
 let randomizerOne = Math.random(); // Creates a random value between 0 and 1
 let randomizerTwo = Math.random(); // Creates a random value between 0 and 1
   
 let extraLat = 0.0004 + (Math.random()*0.001); // Calculates the latitudinal distance (0.0004 = 60m, 0.001 = 90m), to ensure <150m and >60m
 let extraLon = 0.0004 + (Math.random()*0.001); // Calculates the longitudinal distance (0.0004 = 60m, 0.001 = 90m), to ensure <150m and >60m
   
 // Creates a while loop to continuously calculate for new values if the total distance is greater than 150m
 while(((Math.sqrt(Math.pow(extraLat,2) + Math.pow(extraLon,2)))*150000) > 150){
       
  extraLat = 0.0004 + (Math.random()*0.001);
  extraLon = 0.0004 + (Math.random()*0.001);
       
   }
   
   totalDistance = ((Math.sqrt(Math.pow(extraLat,2) + Math.pow(extraLon,2)))*150000).toFixed(2) // Calculates the total distance from the start to the destination using Hypotenuse triangle formula (into meters)
   
   document.getElementById('distance').innerHTML += totalDistance + " m" // Displays the total distance of the random destination from the start
    
   if(randomizerOne > 0.5){ // Checks the random value generated to either add or deduct the distance
       
   newLatitude = lngLat.latitude + extraLat; // Adds the additional distance to the current user latitude
       
   } else{ 
       
       newLatitude = lngLat.latitude - extraLat; // Deducts the additional distance to the current user latitude
   }
    
   if(randomizerTwo > 0.5){ // Checks the random value generated to either add or deduct the distance
       
   newLongitude = lngLat.longitude + extraLon; // Adds the additional distance to the current user longitude
       
   } else{
       
       newLongitude = lngLat.longitude - extraLon; // Deducts the additional distance to the current user longitude
   }
    
 destinationArray = [newLongitude,newLatitude]; // Sets an array containing the coordinates of the destination

 // Creates a popup to display 'New Destination' at the random generated location    
 popup = new mapboxgl.Popup({closeOnClick: false})
 .setLngLat(destinationArray)
 .setHTML('New Destination')
 .addTo(map);
    
 currentLocationArray = [lngLat.longitude,lngLat.latitude]; // Sets an array containing the coordinates of the current location
   
 document.getElementById('startRun').removeAttribute("disabled"); // Enables the 'Start Run' button
    
 newlngLat = { // Sets an object containing the coordinates of the destination as properties
    latitude: newLatitude,
    longitude: newLongitude

    }  
 }

function startRun(){ // Function to start the run as well the time taken

    document.getElementById('startRun').disabled = true; // Disables the 'Start Run' button
    
    document.getElementById('stopRun').removeAttribute("disabled"); // Enables the 'Stop Run' button
    
    document.getElementById('saveRun').removeAttribute("disabled"); // Enables the 'Save' button
    
    // Sets the seconds and minutes into zero
    sec = 0;
    min = 0;
    
    timer = setInterval(theTimer,1000); // Sets an interval function to iterate theTimer function every 1 second
    
    function theTimer() { // Creates an iterating function that counts seconds and minutes additively
        
	sec++; // Increments the seconds
        
	if(sec === 60) { // Checks once the timer reaches 60 seconds
		sec = 0; // Resets the seconds to zero
		min++; // Increments the minutes
      }
        	document.getElementById('time').innerHTML = " "+min+" "+" min "+sec+" "+"sec"; // Displays the duration of the run into the corresponding DIV
     }  
    
}


function stopPath() { // Function that will run when the 'Stop Run' button is pressed
    
        clearInterval(timer) // Stops the timer function from iterating
    
        document.getElementById('stopRun').disabled = true; // Disables the 'Stop Run' button
        document.getElementById('saveRun').removeAttribute("disabled"); // Enables the 'Stop Run' button
    }
