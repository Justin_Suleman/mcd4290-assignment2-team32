// Shared code needed by all three pages.

key = 'runArray' // Creates a variable to be used as a key for storing runs
key1 = 'index' // Creates a variable to be used as a key for storing index of each run

var APP_PREFIX = "monash.mcd4290.runChallengeApp"; // Prefix provided intially

var arrayIndex = -1 // Sets the index to -1 to initialise the index

var localLoad = JSON.parse(localStorage.getItem('runArray')) // Parses the retrieved array of runs from local storage

// Creates an IF statement to check if the local stored array is present
if(localLoad === null){
    
    savedRuns = [] // Create a new empty array 
}else{
    
    savedRuns = localLoad // Set the array equal to the pre-existing local stored array
}

class Run // Creates a class to assign each run as an object with the starting and destination locations, run names, as parameters
{
    constructor (runTitle, newStartLocation, newDestinationLocation, day, time, distanceTravelled, totalDistance)
    {
         let date = new Date // Creates a new date for the run
         
         this._runTitle = runTitle // Sets the initial name of the run
         this._startLocation = newStartLocation; // Sets the initial starting coordinates of the run
         this._destinationLocation = newDestinationLocation; // Sets the initial destination coordinates of the run
         this._day = day; // Sets the initial date of the run
         this._time = time; // Sets the initial duration of the run
         this._distanceTravelled = distanceTravelled // Sets the initial distance travelled of the run
         this._totalDistance = totalDistance // Sets the initial total distance of the run
    }
    
    get totalDistance() // Adds a getter method to return the total distance of the run
    {
        return  this._totalDistance
    }
    
    get distanceTravelled() // Adds a getter method to return the distance travelled of the run
    {
        return  this._distanceTravelled
    }
    
    get runTitle() // Adds a getter method to return the name of the run
    {
        return  this._runTitle
    }
    
    get startLocation() // Adds a getter method to return the starting location of the run
    {
        return  this._startLocation
    }
    
    get destinationLocation() // Adds a getter method to return the destination location of the run
    {
        return this._destinationLocation
    }
    
    get day() // Adds a getter method to return the date of the run
    {
        return this._day
    }
    
    get time() // Adds a getter method to return the duration of the run
    {
        return this._time
    }
}

function savePath(){ // Function that will run when the 'Save' button is pressed
    
    let duration = min + ':' + sec // Assigns the minutes and seconds of the run into a variable
    
    let saveBox = prompt("Save run name:", ''); // Prompts the user to name their saved run
    
    arrayIndex += 1 // Iterates the index after each save
    
    savedRun = new Run(saveBox,lngLat,newlngLat,day,duration,(totalDistance - distanceRemainder),totalDistance) // Creates a new instance of a Run taking the variables created from the new run page
    
    savedRuns.push(savedRun) // Pushes the run instance into the array
    
    localStorage.setItem(key,JSON.stringify(savedRuns)) // Saves the array into local storage after stringifying the array of runs
    
    localStorage.setItem(key1,JSON.stringify(arrayIndex)) // Saves the index of the array into a a different local storage after stringifying
    
    window.location.href = 'index.html' // Transitions to the main page
    
}




