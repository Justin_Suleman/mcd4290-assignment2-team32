// Code for the View Run page.

outPut = JSON.parse(localStorage.getItem('runArray')); // Parses the stringified array of runs from local storage into a variable

thisIndex = localStorage.getItem('runIndex'); // Retrieves the corresponding index of the run in the array

var speed; // Defines a global variable for the average speed

var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun"); // Sets the variable equal to the index of the selected run from local storage

// Calculates the average speed by dividing the distance travelled with the time for the specific run
speed = ((Number(outPut[thisIndex]._distanceTravelled))/(Number((outPut[thisIndex]._time).substring(2,3)) +(Number((outPut[thisIndex]._time).substring(0,1)))*60).toFixed(2)

// Displays the average speed of the indexed run into the corresponding DIV
document.getElementById('speed').innerHTML += 'Speed: ' + speed + ' m/s';

// Displays the starting coordinates of the indexed run into the corresponding DIV
document.getElementById('startPosition').innerHTML += '(' + (outPut[thisIndex]._startLocation.longitude).toFixed(4) + ', ' +(outPut[thisIndex]._startLocation.latitude).toFixed(4) + ')';

// Displays the destination coordinates of the indexed run into the corresponding DIV
document.getElementById('endPosition').innerHTML += '(' + (outPut[thisIndex]._destinationLocation.longitude).toFixed(4) + ', ' +(outPut[thisIndex]._destinationLocation.latitude).toFixed(4) + ')';

// Displays the total distance of the indexed run into the corresponding DIV
document.getElementById('totalDistance').innerHTML += 'Total Distance: ' + outPut[thisIndex]._totalDistance + ' m';

// Displays the date of the indexed run into the corresponding DIV
document.getElementById('date').innerHTML += 'Date: ' + outPut[thisIndex]._day;

// Displays the run time of the indexed run into the corresponding DIV
document.getElementById('time').innerHTML += 'Duration of Run: ' + outPut[thisIndex]._time

// Displays the remaining distance of the indexed run into the corresponding DIV
document.getElementById('remaining').innerHTML += 'Distance Travelled: ' + (outPut[thisIndex]._distanceTravelled).toFixed(2) + ' m'

// Creates a map interface that will be added to the 'map' DIV
map = new mapboxgl.Map({
    
container: 'map', // Container id
style: 'mapbox://styles/mapbox/streets-v11',
center: [-96, 37.8], // Sets the starting position of the map interface
zoom: 15 // Sets the initial zoom of the map interface
    
});

// Sets a new coordinate to mark the current location from the specific saved run
currentPositionSave = new mapboxgl.LngLat(outPut[thisIndex]._startLocation.longitude,outPut[thisIndex]._startLocation.latitude);

// Sets the map to zoom in at the starting location of the specific saved run
map.panTo(currentPositionSave)
var popupCurrent = new mapboxgl.Popup({offset: 20, className: 'my-class'})
        .setLngLat([outPut[thisIndex]._startLocation.longitude,outPut[thisIndex]._startLocation.latitude])
        .setText('Current Location')
        .addTo(map);

// Creates a popup in the map to display the destination location of the selected run
var popup = new mapboxgl.Popup({closeOnClick: false}) 
        .setLngLat([outPut[thisIndex]._destinationLocation.longitude,outPut[thisIndex]._destinationLocation.latitude])
        .setHTML('New Destination')
        .addTo(map);

map.on('load', function () { // Waits for all map elements to load
 
 map.addLayer({ // Adds a line path between the destination and start location on the map
 "id": "route",
 "type": "line",
 "source": {
 "type": "geojson",
 "data": {
 "type": "Feature",
 "properties": {},
 "geometry": {
 "type": "LineString",
 "coordinates": [
 [outPut[thisIndex]._startLocation.longitude,outPut[thisIndex]._startLocation.latitude],[outPut[thisIndex]._destinationLocation.longitude,outPut[thisIndex]._destinationLocation.latitude],
 ]
 }
 }
 },
 "layout": {
 "line-join": "round",
 "line-cap": "round"
 },
 "paint": {
 "line-color": "#888",
 "line-width": 8
 }
 });
 });

function deleteRun(){ // Function that will run when the 'Delete Run' button is pressed
    
    // Deletes all pre-existing data saves from the local storage
    localStorage.removeItem('runArray');
    localStorage.removeItem('runIndex');
    localStorage.removeItem('index');
    localStorage.removeItem('attempt');
    
    outPut.splice(thisIndex,1); // Removes the specific run from the array, by specifying the index for the corresponding run in the array
    
    localStorage.setItem(key,JSON.stringify(outPut)); // Saves the stringified array into local storage after removing the specific run
         
}

function reattemptRun(){ // Function that will run when the 'Reattempt Run' button is pressed
    
    var key2 = 'attempt'; // Creates a variable that will be used as a key
    
    localStorage.setItem(key2,'1'); // Saves the value '1' that will later be used as a signal variable for reattempting
    
    window.location.href = 'newRun.html'; // Transitions to the newRun webpage
     
}

